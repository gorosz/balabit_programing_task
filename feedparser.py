#!/usr/bin/env python
# -*- coding: utf-8 -*-

from bs4 import BeautifulSoup
import xml.etree.ElementTree as ET
import json
import logging
import sys
import traceback
import urllib.request


""" Module to parse rss and atom feeds from html file """


logging.basicConfig(filename='exception.log', format='%(asctime)s - %(message)s')
logger = logging.getLogger('feedparser')


def log_traceback(ex, ex_traceback=None):
    """ Log trace stack into file """
    if ex_traceback is None:
        ex_traceback = ex.__traceback__
    tb_lines = [line.rstrip('\n') for line in traceback.format_exception(ex.__class__, ex, ex_traceback)]
    # print(tb_lines)
    logger.exception(ex)
    # sys.exit(1)


def read_html_file(source_file):
    """ Read the input.html file and return list of lines """
    try:
        with open(source_file) as sf:
            return sf.readlines()
    except Exception as ex:
        log_traceback(ex)


def get_feed_links(html_lines):
    """ Parse the read html file and get all the urls """
    soup = BeautifulSoup(''.join(html_lines))
    anchor_urls = [l['href'] for l in soup.find_all('a')]
    link_urls = [l['href'] for l in soup.find_all('link')]
    return anchor_urls + link_urls


def feed_type(url):
    """ Check the feed type """
    try:
        with urllib.request.urlopen(url) as page:
            root = ET.fromstring(page.read())
            if root.tag == 'rss':
                return 'rss'
            else:
                return 'atom'
    except Exception as ex:
        log_traceback(ex)
        return 'invalid'


def collate_feeds(feed_urls):
    """ Create a dict by feed type """
    collated_feeds = {"rss": [], "atom": []}
    for l in feed_urls:
        ft = feed_type(l)
        if ft == 'atom':
            collated_feeds['atom'].append(l)
        elif ft == 'rss':
            collated_feeds['rss'].append(l)
        else:
            continue
    return collated_feeds


def write_to_json(obj):
    """ Convert and write the dict to json file """
    try:
        json_obj = json.dumps(obj)
        try:
            with open('output.json', 'w') as of:
                of.write(json_obj)
        except Exception as ex:
            log_traceback(ex)
    except Exception as ex:
        log_traceback(ex)


def main():
    print('Staring')
    html_lines = read_html_file('input.html')
    links = get_feed_links(html_lines)
    collated_feeds = collate_feeds(links)
    write_to_json(collated_feeds)
    print('Finished')


if __name__ == '__main__':
    sys.exit(main())
